/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dataConvert;

import java.io.*;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author frendhisaidodanaro
 */
public class ResultToSql {

    public void convertToSql(String[] fileInputs, String NamaTable){
        
       for(String eachFile : fileInputs){ 
        try{
            String namaFile = eachFile.substring(0, eachFile.indexOf(".")); //trim nama file untuk nama output&orientasi
            
            FileInputStream fstream = new FileInputStream(eachFile); //buka file
            
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            BufferedWriter writer = new BufferedWriter(new FileWriter( namaFile+".sql" ,true)); //output jadi namafile.sql
            
            
            
           
            String strLine, date, content;
           
            while ((strLine = br.readLine()) != null) {
                writer.write("INSERT INTO  `"+NamaTable+"` (`datetime`,`orientasi` ,`content`) VALUES ");
                date = strLine.substring(0, 20).replace("T"," ").replace("Z", ""); //DATE
                content = strLine.substring(22); //Content
                
                writer.append("('"+ date +"', '" + namaFile + "','"+ content +"');"+"\n");
                //writer.newLine();
                //System.out.println("('" + fields[0] + "' , '" + fields[1]  + "' , '" + fields[2] +"'),"
                //    + "\n");
            }
            writer.close();
            in.close();
            br.close();
            fstream.close();
            
        
        }catch (Exception e){
            System.out.println(e);
        }
       }
    }
    
    
    public void generateRandom(String fileInput, String namaFileOutput, String NamaDatabase, String NamaTable){
        
        try{
            
            
            Random randGen = new Random(20);
            Set<String> randnum = new HashSet<String>(1000);
            for (int idx = 1; idx <= 1000; ++idx){
            int randomInt = randGen.nextInt(13709);
            //log("Generated : " + randomInt);
            String tempRand = ""+randomInt;
            randnum.add(tempRand);
            }
            
            //String orientasi = fileInputs[i].substring(0, fileInputs[i].lastIndexOf('.'));
            //System.out.println("Building INSERT query for "+orientasi);
            FileInputStream fstream = new FileInputStream(fileInput);
            
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            BufferedWriter writer = new BufferedWriter(new FileWriter( namaFileOutput ,true));
            BufferedWriter writer2 = new BufferedWriter(new FileWriter( ("trim_"+fileInput) ,true));
            String strLine;
            
            writer.write("INSERT INTO `"+NamaTable+"` (`id` ,`tweet` ,`orientasi`) VALUES ");
            writer.newLine();
            
            int lng;
            int counter=0;
            while ((strLine = br.readLine()) != null) {
                //fields[0] = strLine.substring(0, 10); //DATE
                //fields[1] = orientasi; //Orientasi
                //fields[2] = strLine.substring(20); //Content
                counter++;
                String cekCount = ""+counter;
                if(randnum.contains(cekCount)){
                strLine = strLine.substring(22);                
                System.out.println(counter+" : "+strLine);
                writer.append("(NULL , '" + strLine  + "', NULL),"+"\n");
                }else{
                writer2.write(strLine);
                writer2.newLine();
                }
                
                //writer.newLine();
                //System.out.println("('" + fields[0] + "' , '" + fields[1]  + "' , '" + fields[2] +"'),"
                //    + "\n");
            }
            writer.close();
            writer2.close();
            in.close();
            br.close();
            fstream.close();
            
        
        }catch (Exception e){
            System.out.println(e);
        }
    }
    private static void log(String aMessage){
    System.out.println(aMessage);
    }
    public static void main(String[] a) {
        
        
        
        String NamaTable        = "data",
               //NamaFileInput    = "classresult.txt",
               NamaFileSQLout   = "sqldataset";
        String [] inputs = new String[3];
        inputs[0] = "nonopini.txt";
        inputs[1] = "negatif.txt";
        inputs[2] = "positif.txt";
        
        ResultToSql tp = new ResultToSql();
        
       
        tp.convertToSql(inputs, NamaTable);
       
    
    }

}
