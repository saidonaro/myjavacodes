/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dataConvert;

import java.io.*;
import java.util.*;


/**
 *
 * @author frendhisaidodanaro
 */
public class SplitPerDate {
    
    public void splitEm(String namaFolder,String fileInput, ArrayList<String> listDate) {
        try{
            String namaFile = fileInput.substring(0, fileInput.indexOf("."));
            System.out.println("Reading "+ fileInput);
            FileInputStream fstream = new FileInputStream(fileInput);
            
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            
            PrintWriter[] arrPw = new PrintWriter[listDate.size()]; 
            for (int i=0;i<listDate.size();i++) {
            	PrintWriter pw = new PrintWriter(namaFolder+namaFile+"_"+listDate.get(i)+".txt");
            	arrPw[i] = pw;
            }
            
            
            String strLine,cekDate;
            int getDate;
            try{
            while ((strLine = br.readLine()) != null) {
                System.out.println(strLine);
                cekDate = strLine.substring(0,13).replace("T"," ");
                System.out.println("cekDate "+cekDate);
                getDate = listDate.indexOf(cekDate);
                System.out.println("getDate "+getDate);
                arrPw[getDate].println(strLine);
            
            }
            }catch(Exception e){
                System.out.println("here "+e);
            }
            
            
            for (int i=0;i<listDate.size();i++) {
            	arrPw[i].close();
            }
        }catch(Exception e){
            System.out.println(e.toString());
        }
    }
    
    public ArrayList<String> getDates(String filename){
        ArrayList<String> listTanggal = new ArrayList<String>();
        String strLine;
        try{
         FileInputStream fstream = new FileInputStream(filename);
            
         DataInputStream in = new DataInputStream(fstream);
         BufferedReader br = new BufferedReader(new InputStreamReader(in));
         
         while ((strLine = br.readLine()) != null) {
            listTanggal.add(strLine.substring(1, (strLine.length()-1)));
            }
        }catch( Exception e){
         System.out.println(e.getMessage());   
        }
        
        return listTanggal;
    }
    
    public ArrayList<String> collectTime(String filename, boolean isPerhour){
        Set collect = new LinkedHashSet();
        ArrayList<String> listTanggal = new ArrayList<String>();
        String strLine;
        int subindex = isPerhour ? 13 : 10;
        try{
         FileInputStream fstream = new FileInputStream(filename);
            
         DataInputStream in = new DataInputStream(fstream);
         BufferedReader br = new BufferedReader(new InputStreamReader(in));
         String cekDate;
            while ((strLine = br.readLine()) != null) {

                cekDate = strLine.substring(0,subindex).replace("T"," ");

                    if(collect.add(cekDate)){
                        listTanggal.add(cekDate);
                        System.out.println(cekDate);
                    }

            }
            
        System.out.println("Total: "+ listTanggal.size());
        System.out.println("_____________________________");
        }catch( Exception e){
         System.out.println(e.getMessage());   
        }
        
        return listTanggal;
    }
            
    public static void main(String[] a) {
        SplitPerDate sp = new SplitPerDate();
        
        //ArrayList<String> listDates = sp.getDates("datadates_jam.txt");
        ArrayList<String> listNeg = sp.collectTime("negatif.txt",true);
        ArrayList<String> listPos = sp.collectTime("positif.txt",true);
        ArrayList<String> listNon = sp.collectTime("nonopini.txt",true);
        //System.out.println(listDates.indexOf("2012-03-24 19"));
        
        sp.splitEm("perhour_negatif/","negatif.txt", listNeg );
        sp.splitEm("perhour_positif/","positif.txt", listPos );
        sp.splitEm("perhour_nonopini/","nonopini.txt", listNon );
        
        
    }
}
