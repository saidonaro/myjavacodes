package dataConvert;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
//import java.io.Writer;

public class XMLconvertTo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
                String fileInput = "data.xml";
                String fileOutput = "data2.text";
		try{
			File XMLfile = new File(fileInput);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(XMLfile);
			doc.getDocumentElement();
                        BufferedWriter writer = new BufferedWriter(new FileWriter(fileOutput,true));
			System.out.println("Root Element : " + doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName("Tweet");
                        
                        int fileLength = nList.getLength();
			System.out.println("Panjang: "+fileLength);
                        
                        int temp;
                        int lines = 0;
                        String tempTweet;
			for (temp = 0; temp < fileLength; temp++) {
				
				Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						
						Element eElement = (Element) nNode;
						if(checking(getTagValue("Content",eElement))){
                                                    tempTweet = cleaning(getTagValue("Content",eElement));
                                                    writer.write(getTagValue("Time",eElement)+"||"+tempTweet);
                                                    writer.newLine();
                                                    lines++;
                                                }
						
					}		
			}
			System.out.println("Success! "+fileOutput+" has "+lines+" new lines.");
                        System.out.println("DIRTY:"+ (fileLength-lines));
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
        private static boolean checking(String atweet) {
            atweet = atweet.toLowerCase();
            if(atweet.contains("kirim ke") || atweet.length() < 10){
                return false;
            }                              
            return true;
        }
        
	private static String cleaning(String atweet) {
           
                atweet = atweet.toLowerCase();                  //casefolding
                //atweet = atweet.replaceAll("#[\\w|:_]*", " ");  //buang #xxxx hashtag
                atweet = atweet.replaceAll("@[\\w|:_]*", "zmentionz");  //buang @xxxx
                //atweet = atweet.replaceAll("http://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]"," "); // buang url
                //atweet = atweet.replaceAll("[^a-z0-9 ]", " "); // buang selain a z, 0-9
                atweet = atweet.replaceAll("\n", " ");
                atweet = atweet.replaceAll("\\b\\s{2,}\\b", " "); //buang multiple spaces dengan 1 blankspace
            
            return atweet;
        }
        
        private static String getTagValue(String sTag, Element eElement) {
		NodeList nList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
		if(sTag.equalsIgnoreCase("twitter:geo")){
			Node nValue = (Node) nList.item(0);
			if(nValue.getNodeValue().isEmpty()){
				return null;
			}
		}
		Node nValue = (Node) nList.item(0);
		return nValue.getNodeValue();
	}

}
