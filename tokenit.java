package dataConvert;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.io.LineNumberReader;


/**
 * Program hitung TFIDF
 * 
 * @author frendhisaidodanaro
 */
public class tokenit {

    // Fungsi sorting TreeMap berdasarkan value.
    static <K,V extends Comparable<? super V>> SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
        SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
            new Comparator<Map.Entry<K,V>>() {
                @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                    int res = e1.getValue().compareTo(e2.getValue());
                    return res != 0 ? res : 1; 
                }
            }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }
    

    
    
    public void process(String outputdir,String dirInput, String fileInput) {

        
            String namaFile = fileInput.substring(0, fileInput.indexOf(".")); 
            String orientasi = namaFile.substring(0, namaFile.indexOf("_"));
            String tanggal = namaFile.substring(namaFile.indexOf("_")+1,namaFile.length());
            
            String outputFile = outputdir+orientasi;
            
            
            //System.out.println("namaFile= "+namaFile+" ornt:"+orientasi+" tgl:"+tanggal);
            fileInput = dirInput+"/"+fileInput;
            
            int totalTerms = 0;
            int totalDoc;

            
            //
            ArrayList<HashMap<String, Integer>> arrTweets = new ArrayList<HashMap<String, Integer>>();
            
            ArrayList<HashMap<String, Double>> arrTFIDF = new ArrayList<HashMap<String, Double>>();
            
            HashMap<String, Integer> docFreq = new HashMap<String, Integer>();
            
            TreeMap<String, Double> tfIDF = new TreeMap<String, Double>();
            
            
          
        try{
            // Tulis hasil hitung rata-rata ke file output namafile_tfidf_stat.txt
                   
            FileInputStream fstream = new FileInputStream(fileInput);
            
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            System.out.println("Reading "+ fileInput);
            
            /*
             * HITUNG TERM FREQUENCY
             * Membaca file input
             * Mencari jumlah tf tiap term per baris
            */
            String strLine;
            Integer tfreq;
            while ((strLine = br.readLine()) != null) {
                HashMap<String, Integer> termFreq = new HashMap<String, Integer>();
                
                String docn = strLine.substring(22,strLine.length());
                
                    Scanner sc = new Scanner(docn);
                    
                    while(sc.hasNext()) { 
                        String term = sc.next();
                        
                        if(!term.equalsIgnoreCase("indosat")){ //Skip keyword indosat, karena ada di setiap tweet.
                            tfreq = termFreq.get(term); //Ambil value
                            termFreq.put(term, (tfreq == null) ? 1 : tfreq + 1); //Jika value masih kosong, isi 1. Jika 1, increment.
                            totalTerms++;
                        }
                    }
                    sc.close();
                    arrTweets.add(termFreq);//Simpan termFreq.
            }
            br.close();
            /*
            * Selesai membaca dataset.
            * arrTweet berisi HashMap termFreq, tiap termFreq adalah representasi dokumen/tweet, berisi jumlah tf dari masing2 term.
            */
            
            
            /*
            * HITUNG DOCUMENT FREQUENCY
            * Iterasi arrTweets, untuk menghitung df.
            * Menghitung jumlah dokumen yang mengandung term.
            * docFreq.put("awan",7) 
            * Artinya term "awan", ditemukan di 7 dokumen/tweet
            * 
            */
            Iterator iterArray = arrTweets.iterator();
            while(iterArray.hasNext()){
                HashMap perTweet = (HashMap) iterArray.next();
                
                Iterator iterEach = perTweet.keySet().iterator();
                    while(iterEach.hasNext()){
                        String eachW = (String) iterEach.next();
                       
                    }
            }
            // Selesai menghitung DF tiap term
            // HashMap docFreq berisi key= term, value= document frequency
            
           // HITUNG IDF dan TFIDF
           // arrTweets sekali lagi di iterasi
           // untuk menghitung nilai IDF lalu sekaligus dihitung TF*IDF nya
           // di tiap dokumen nilai TF*IDF per term dihitung, dan disimpan di HashMap valTFIDF
           // lalu valTFIDF ini dikumpulkan di arrTFIDF,\
           Iterator iterTF = arrTweets.iterator();
           Double idf,tfidf;
           totalDoc = arrTweets.size();
            while(iterTF.hasNext()){
                HashMap<String, Double> valTFIDF = new HashMap<String, Double>();
                HashMap perTweet = (HashMap) iterTF.next();
                
                Iterator iterEach = perTweet.keySet().iterator();
                while(iterEach.hasNext()){
                    String aTerm = (String) iterEach.next(); //ambil term yang akan diproses
                    Integer dfreq = docFreq.get(aTerm); //ambil nilai DF dari term yang akan diproses
                    
                        if(dfreq>1){
                            Integer cfreq = (Integer) perTweet.get(aTerm); // ambil nilai tf dari aTerm
                            idf = Math.log(totalDoc/dfreq); 
                            tfidf = cfreq * idf;
                            valTFIDF.put(aTerm, tfidf);
                            //System.out.println("TFIDF("+aTerm+")= "+cfreq+" * "+"log("+totDoc+"/"+dfreq+") = "+ tfidf+" , ");
                        }
                }
                arrTFIDF.add(valTFIDF); //Selesai olah satu perTweet, simpan HashMap valTFIDF ke arrTFIDF
            }
            // Selesai hitung IDF dan TF*IDF
            // arrTFIDF berisi nilai tfidf tiap term per dokumen, yaitu valTFIDF
            
            // Tulis hasil hitung TF*IDF ke file output namafile_tfidf.txt
                BufferedWriter writeTFIDF = new BufferedWriter(new FileWriter( (outputFile+"_tfidf.txt") ,true));
                Iterator iterValTFIDF = arrTFIDF.iterator();
                System.out.println("writeTFIDF opened "+outputFile+"_tfidf.txt");

                while(iterValTFIDF.hasNext()){
                    HashMap perTweet = (HashMap) iterValTFIDF.next();

                    //System.out.println(perTweet.toString());
                    Iterator iterEach = perTweet.keySet().iterator();
                    while(iterEach.hasNext()){
                        String aTerm = (String) iterEach.next();
                        Double valTFIDF = (Double) perTweet.get(aTerm);
                        writeTFIDF.write(aTerm+"="+valTFIDF+"; ");
                        //System.out.print(aTerm+"="+valTFIDF+"; ");
                    }
                    //System.out.println("__");
                    //writeTFIDF.newLine();
                }
                writeTFIDF.close();
            
            // Hitung rata-rata bobot TFIDF term, jika denganStat= true
            
                    // HITUNG jumlah rata2 TFIDF tiap term
                    for(String word : docFreq.keySet()){
                            Integer dfreq = docFreq.get(word);

                            if(dfreq>1){ //hanya hitung term yang muncul di lebih dari satu dokumen 

                                //System.out.println("Collecting term: "+word+" df= "+dfreq);
                                Double tfIDFstat = 0.0; // Inisiasi nilai tfIDFstat, digunakan untuk akumulasi
                                int cc=0;
                                Iterator iterTFIDF = arrTFIDF.iterator();
                                while(iterTFIDF.hasNext()) {
                                    HashMap val = (HashMap) iterTFIDF.next();

                                    if(val.containsKey(word)){
                                            for(Object t : val.keySet()) {

                                                if(t.toString().equals(word)){
                                                    cc++;
                                                    tfIDFstat = tfIDFstat + (Double) val.get(word); //akumulasi nilai tfidf suatu term di seluruh dokumen
                                                }
                                            }
                                    }
                                }
                                //System.out.println("Counted="+cc+"   tfIDFstats="+tfIDFstat);
                                Double tfIDFtot = tfIDFstat/cc;                                 //HITUNG RATA-RATA
                                //System.out.println("tfidf("+word+")="+tfIDFtot);
                                tfIDF.put(word, tfIDFtot); //Simpan di TreeMap tfIDF
                            }
                    }

                    
                  
                            
             
            
        }catch(Exception e){
            System.out.println(e.toString());
        }finally{   
            System.out.println("unik: "+docFreq.size());
            //System.out.println("Jumlah document:"+ arrTweets.size());
            System.out.println("Total term: "+totalTerms);
        }
    }
    
 
 
 
            
    public static void main(String[] a) {
         
        
        procTFIDF pt = new procTFIDF();
        
        
    }
}
