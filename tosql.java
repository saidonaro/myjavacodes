/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dataConvert;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author frendhisaidodanaro
 */
public class tosql {
    
    public void process(String fileInput){
        
        try{
            FileInputStream fstream = new FileInputStream(fileInput);
            
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            System.out.println("Reading "+ fileInput);
            
            BufferedWriter writeFile = new BufferedWriter(new FileWriter( (fileInput+".sql") ,true));
            
            /*
             * HITUNG TERM FREQUENCY
             * Membaca file input
             * Mencari jumlah tf tiap term per baris
            */
            String strLine;
            Integer tfreq;
            
            writeFile.write("INSERT INTO `synword` (`key`,`word`,`freq`,`syn_key`) VALUES ");
            writeFile.newLine();
            while ((strLine = br.readLine()) != null) {
                
                String oneWord = strLine.substring(2,strLine.indexOf(" ("));
                String jumlah = strLine.substring((strLine.indexOf(" (")+2),strLine.indexOf(")"));
                String inserts = "(NULL,'"+oneWord+"',"+jumlah+",NULL),";
                writeFile.write(inserts);
                writeFile.newLine();
                    
            }
            
            writeFile.close();
            br.close();
            in.close();
            fstream.close(); 
            
    }catch(Exception e){
        
    }
    }
    
    public static void main(String[] a) {
         
        
    tosql ts = new tosql();
    
    ts.process("words.txt");
        
        
    }
    
}
